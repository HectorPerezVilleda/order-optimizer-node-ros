## Topics description
- **Topic name:** currentPosition.
    - **Description:** Receive the position of the robot, considering only planar position      (x,y). Orientation is not considered. 

    - **Type of message:** PoseStamped.

    - **Fomat:** `'{ header: { frame_id: "/map" }, pose: { position: { x: 0.25, y: 0 }, orientation: { x: 0, y: 0, z: 0, w: 1 } } }'`


- **Topic name:** nextOrder
    - **Description:** Receive the order that contains the part to search and pick up
    - **Type of message:** Custom message.
    
    - **Fomat:** `{"order_id: 100, description: 'Description of the order' "}`

- **Topic name:** oder_path
    - **Type of message:** MarkerArray
    



## Runinig steps:

- **Run the node:**

     `rosrun order_optimizer OrderOp`.

- **Publish robot Position**

    `rostopic pub -1 /currentPosition geometry_msgs/PoseStamped '{ header: { frame_id: "/map" }, pose: { position: { x: 0.25, y: 0 }, orientation: { x: 0, y: 0, z: 0, w: 1 } } }'`


- **Publish the order**

    `source devel/setup.bash`

    `rostopic pub -1 /nextOrder order_optimizer/Custom_msg {"order_id: 100, description: 'Description_1' "}`


- **Get Position of Robots and PickUp parts**

    `source /opt/ros/noetic/setup.bash`

    `rostopic echo /order_path `


## Notes:
- Before publish the order, the position of the robot is required in order to calculate the shortest path. The shortest path is calulated with the current robot position.
- The "orders" folder and the "configuration" folder are located in /src/containing_files
- The assignment says there are several yaml files for the orders but it does not specify anything about the name structure. I have named them with the following convention "order_day_0.yaml, order_day_1.yaml, order_day_2.yaml,...". I have included three files.
- The path of the "containing_files" folder is provided in a `string` variable named "Path_file"


## Pseudo Code:
$\mathbf{Required}$:
- robotPose: $R_p\in\mathbb{R}^{2}$ (currentPosition)
- nextOrder: [$\mathrm{orderid}$,  $\mathrm{description}$] (Custom message )
- Path: Path of "containing_files" folder

cntrFile=0

```cpp
try{
    while(FileExist==True){
        // List that store the set of parts
        ListOfPartsToPickUp[]
        // Define file to search the desired order: 
        OrderFile=order_day_+str(cntrFile)
        Update ROS parameters from OrderFIle.yaml
        if OrderFile exist:
            Search nextOrder in  OrderFile
            if nextOrder found:
                // Get the list of products of the founded order
                List_Products=GetListProducts()
                for each_product in List_Products:
                    // Search each product of the List of products in the Products.yaml file
                    search each_product in Products.yaml
                    for each_part in Product of Products.yaml
                        Part=each_product.get_part()
                        ListOfPartsToPickUp.push_back(Part)
            else:
                //If the order was not found in the current file, increase the counter to search in the next file
                cntrFile++
        else:
            // The order was not found in any file
            FileExist==False
            throw NoFile;        
    }
    //Calculate the shortest path to Pick up all the parts of the list 
    SortedPartsList=findShortestPath(ListOfPartsToPickUp)
    //Publish the Shorted Part List in oder_path Topic
    Publish(SortedPartsList)

}
catch(int NoFile) 
{
    return: "Non Order found"
}
//function that generates all the possible permutations of the list of parts, calculate the Euclidian distance of each perutation and return the permutation with the minimum Eculidian distance
//For each permutation, the initial vertex is always the robot position and the final vertex is the point to be delivered
SortedPartsList ShortestPath(List_Parts)
{
    // initialize the Distance value with large value
    Diistance=inf
    //GEnerate each permutation
    for each Permutation in List_Parts:
        // Get the Euclidian Distance for each Permutation
        Distance_permutation=getEuclidianDistance(CurrentPermutation)
        // If the Euclidian Distance of the current permutation is smaller than the current distance, then update the Path to return
        if Distance_permutation<Distance
            Distance=Distance_permutation
            SortedPartList=CurrentPermutation
    return CurrentPermutation



}

```


## Running Test:  

- **Publish the robot position:**

    - rostopic pub -1 /currentPosition geometry_msgs/PoseStamped '{ header: { frame_id: "/map" }, pose: { position: { x: 8.25, y: 1.34 }, orientation: { x: 0, y: 0, z: 0, w: 1 } } }'


- **Publish the order:**

    -`rostopic pub -1 /nextOrder order_optimizer/Custom_msg {"order_id: 111, description: 'This is the description of order 111' "}`.

- **Pick Up path:**
```
    [ INFO] [1671374954.025502388]: Working on order: 111(This is the description of order 111)
    [ INFO] [1671374954.026265708]: Minimum Distance with the shortest path: 479.771
    [ INFO] [1671374954.026285567]: Fetchign part b for product 56 at x: 11.2 at y: 240.8
    [ INFO] [1671374954.026296363]: Fetchign part a for product 56 at x: 120.2 at y: 20.8
    [ INFO] [1671374954.026307543]: Fetchign part d for product 4 at x: 18.12 at y: 13.2
    [ INFO] [1671374954.026322133]: Fetchign part c for product 78 at x: 8.1 at y: 10.2
    
```

- **Published Markers:**
```
   type: 1
    action: 0
    pose: 
      position: 
        x: 8.25
        y: 1.34
        z: 0.0

   type: 3
    action: 0
    pose: 
      position: 
        x: 11.2
        y: 240.8
        z: 0.0

   type: 3
    action: 0
    pose: 
      position: 
        x: 120.2
        y: 20.8
        z: 0.0

   type: 3
    action: 0
    pose: 
      position: 
        x: 18.12
        y: 13.2
        z: 0.0

   type: 3
    action: 0
    pose: 
      position: 
        x: 8.1
        y: 10.2
        z: 0.0


```