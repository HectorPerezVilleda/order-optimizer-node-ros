
#include "ros/ros.h"
#include "std_msgs/String.h"
#include "order_optimizer/Custom_msg.h"
#include <stdlib.h>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;
#include <stdio.h>
#include "geometry_msgs/PoseStamped.h"
#include <visualization_msgs/Marker.h>
#include "visualization_msgs/MarkerArray.h"



ros::Publisher Marker_pub;


// Structure to store the Fetching Part
struct PartToPickUp
{
  string name;
  int Product;
  double cx;
  double cy;

};
//Structure to store Robot pose and share it with a different calback
struct RobotPose
{
  double x;  double y; float z; float qx; float qy;  float qz;  float qw;
} ;
RobotPose RobotPosition;

// List of Part to Pick up
vector<PartToPickUp> ListPartToPickUp;
// Vector: Point (cx,cy) where the order has to be delivered
vector <double> DeliveryOrderPoint;

//Function to evaluate the total cost distance of a given permutation sequence of pickup points
float EvaluatePointsPermutation(vector<PartToPickUp> PartsVector, struct RobotPose InitialRobotPosition, vector<int> PartPickUpSequence)
{

  // ROS_INFO_STREAM("Received position RX: " << RobotPosition.x);
  float DistanceToPoint;
  float P0_x, P0_y, P1_x, P1_y; 
  //Evaluate the Initial robot position with the first part to pick up
  P0_x=InitialRobotPosition.x;
  P0_y=InitialRobotPosition.y;
  P1_x=PartsVector[0].cx;
  P1_y=PartsVector[0].cy;
  // ROS_INFO_STREAM("Points to evalaute (iNITIAL rOBOT pOSE): " <<P0_x<< "  " << P0_y<< "and "<< P1_x<< "  "<< P1_y);
  DistanceToPoint=sqrt(pow((P0_x-P1_x),2)+pow((P0_y-P1_y),2));
  //Evaluate distance for the original order of the points to pick up
  for (int i = 0; i < PartPickUpSequence.size()-1; i++)
  {
    // ROS_INFO_STREAM("Part_Id[i]: " <<PartPickUpSequence[i]);
    // ROS_INFO_STREAM("Part_Id[i+1]: " <<PartPickUpSequence[i+1]);
    P0_x=PartsVector[PartPickUpSequence[i]].cx;
    P0_y=PartsVector[PartPickUpSequence[i]].cy;
    P1_x=PartsVector[PartPickUpSequence[i+1]].cx;
    P1_y=PartsVector[PartPickUpSequence[i+1]].cy;
    // ROS_INFO_STREAM("Points to evalaute: " <<P0_x<< "  " << P0_y<< "and "<< P1_x<< "  "<< P1_y);
    DistanceToPoint+=sqrt(pow((P0_x-P1_x),2)+pow((P0_y-P1_y),2));


  }
  P0_x=PartsVector[PartPickUpSequence.back()].cx;
  P0_y=PartsVector[PartPickUpSequence.back()].cy;
  P1_x=DeliveryOrderPoint[0];
  P1_y=DeliveryOrderPoint[1];
  DistanceToPoint+=sqrt(pow((P0_x-P1_x),2)+pow((P0_y-P1_y),2));
  // ROS_INFO_STREAM("Points to evalaute (LAST POINT): " <<P0_x<< "  " << P0_y<< "and "<< P1_x<< "  "<< P1_y);

  // ROS_INFO_STREAM("TOTAL DISTANCE: " <<DistanceToPoint);
  // Generate all permutations
  return DistanceToPoint;


}
//Function to get the order of Parts to Pick up with the minimum path length

void SmallerPath(struct RobotPose InitialRobotPosition , vector<PartToPickUp> PartsVector,vector <double> DeliveryOrderPoint)
{
  vector<int> Optimal_sequence; // Vector to store the optimal permutation sequence
  float minimumDistance; //Minimum distance that the robot perform with the Optimal Sequence
  vector<int> Part_Id;          // Vector to generate all different permutation sequence
  // Create an Index Part to generate all ID Parts Permutations
  for (int i = 0; i < PartsVector.size(); i++)
  {
    Part_Id.push_back(i);
  }
  float DistanceR;
  //Evaluate initial Permutation
  DistanceR=EvaluatePointsPermutation(PartsVector, InitialRobotPosition, Part_Id);
  // ROS_INFO_STREAM("TOTAL DISTANCEROBOT: " <<DistanceR);
  Optimal_sequence=Part_Id;
  minimumDistance=DistanceR;
  //Generate all permutations and evaluate them
  while (next_permutation(Part_Id.begin(), Part_Id.end()))
  {
    DistanceR=EvaluatePointsPermutation(PartsVector, InitialRobotPosition, Part_Id);
    if (DistanceR<minimumDistance)
    {
      minimumDistance=DistanceR;
      Optimal_sequence=Part_Id;
    }
    
    // ROS_INFO_STREAM("TOTAL DISTANCEROBOT: " <<DistanceR);
  };

  //Create Marker array to publish
  visualization_msgs::MarkerArray MarkerArrayPub; 
  visualization_msgs::Marker marker;

  //Marker por robot position
  marker.type = visualization_msgs::Marker::CUBE;
  marker.pose.position.x=InitialRobotPosition.x;
  marker.pose.position.y=InitialRobotPosition.y;
  MarkerArrayPub.markers.push_back(marker);

  ROS_INFO_STREAM("Minimum Distance with the shortest path: " <<minimumDistance);
  // ROS_INFO_STREAM("SHORTEST PATH: ");
  for (int i = 0; i < Optimal_sequence.size(); i++)
  {
    ROS_INFO_STREAM("Fetchign part " <<PartsVector[Optimal_sequence[i]].name<<
                    " for product "  <<PartsVector[Optimal_sequence[i]].Product<<
                    " at x: "        <<PartsVector[Optimal_sequence[i]].cx<<
                    " at y: "        <<PartsVector[Optimal_sequence[i]].cy);
    //Marker for Parts to PickUp
    marker.type = visualization_msgs::Marker::CYLINDER;
    marker.pose.position.x=PartsVector[Optimal_sequence[i]].cx;
    marker.pose.position.y=PartsVector[Optimal_sequence[i]].cy;
    MarkerArrayPub.markers.push_back(marker);

    
  }
  // Publish the Marker Array
  Marker_pub.publish(MarkerArrayPub);


  
  
}


float q0;
void PoseRobotCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  RobotPosition.x=msg->pose.position.x;
  RobotPosition.y=msg->pose.position.y;
}


void chatterCallback(const order_optimizer::Custom_msg::ConstPtr& msg) {

  // ROS_INFO("Order to search: %d",msg->num);
  //Files path
  string Path_file="rosparam load src/order_optimizer/src/containing_files";
  int file_id=0; // File id of orders
  string Order_file_name;    //Concatenated File name. Path_file+"/orders/order_day_"+file_ide+.yaml
  string Products_file_name=Path_file+"/configuration/Products.yaml"; //Concatenated File name. Path_file+"/configuration/Products.yaml"
  int ProductNum;
  int NoFile=0; //Flag to detect unexisting files
  
  ros::NodeHandle n2,n3;
  int order_;
  //Open Product yaml file
  system(Products_file_name.c_str());
  XmlRpc::XmlRpcValue ProductList;
  n2.getParam("products", ProductList);


  // SEARCH THE ORDER IN EACH FILE
  try
  {
    ListPartToPickUp.clear(); // Clear the list of Part to Pick up in each order
    DeliveryOrderPoint.clear();
    while(NoFile==0)
    {
      //Concatenate Path file with ID
      // ROS_INFO("File ide to open= %d", file_id);
      //Concatenate Path file with ID
      Order_file_name=Path_file+"/orders/order_day_"+to_string(file_id)+".yaml";
      // Open Yaml file of orders  
      NoFile=system(Order_file_name.c_str());
      //If the file exist
      if (NoFile==0)
      {
        // Store file parameters in a list 
        XmlRpc::XmlRpcValue OrderList;
        n2.getParam("orders", OrderList);
        // SEARCH THE ORDER IN THE ORDER FILE
        for (int i = 0; i <OrderList.size(); i++) 
        {
          XmlRpc::XmlRpcValue sublist = OrderList[i];
          order_=sublist["order"];
          // ROS_INFO("this is the orderrrr= %d",order_);
          // If the order is found, extract the 
          if(msg->order_id==order_)
          {
            ROS_INFO_STREAM("Working on order: " <<order_<< "("<<msg->description<<")");
            // Store the Delivery order point
            DeliveryOrderPoint.push_back(sublist["cx"]);
            DeliveryOrderPoint.push_back(sublist["cy"]);
 
            // GET THE LIST OF PRODUCTS OF THE FOUND ORDER
            for (int j = 0; j < sublist["products"].size(); j++)
            { //Sublist that containts each products of the found order
              ProductNum=sublist["products"][j];
              // ROS_INFO("Product of the order= %d",ProductNum);
              // Run over the product list in the Products.yaml file
              for (int k = 0; k < ProductList.size(); k++)
              {
                XmlRpc::XmlRpcValue Productsublist = ProductList[k];
                int Product_id;
                //Product from the Product list (Products.yaml)
                Product_id=Productsublist["product"];
                // ROS_INFO("Product FIle: Product=%d", Product_id);
                // if Product from order list is found in Product list from product.yaml, 
                // then get the parts
                if (ProductNum==Product_id)
                {
                  // ROS_INFO("PArts SIze= %d",Productsublist["parts"].size());
                  // XmlRpc::XmlRpcValue PartsSublist =Productsublist["parts"];

                  //Access to each part of the Product in the Product.yaml file
                  for (int l = 0; l < Productsublist["parts"].size(); l++)
                  {
                   
                    XmlRpc::XmlRpcValue PartsSublist =Productsublist["parts"][l];
                    // Store each part in the List of parts to Pick up
                    ListPartToPickUp.push_back(PartToPickUp());
                    ListPartToPickUp.back().cx=PartsSublist["cx"];
                    ListPartToPickUp.back().cy=PartsSublist["cy"];
                    ListPartToPickUp.back().name=string(PartsSublist["part"]);
                    ListPartToPickUp.back().Product=Product_id;

                  }
                  break; //If the product was found, dont continue runing over the Product list
                  

                }
              }
              
            }
            NoFile=555; // Get out from the While
            break; //If the order was found, dont continue searching for the order in the next file
          }
        }

        file_id++;
      }
      else
      {
        throw NoFile;
      }
              
    }
    // ROS_INFO("DISPLAY PARTS");
    // for (int i = 0; i < ListPartToPickUp.size(); i++)
    // {
    //   ROS_INFO("part name %s", ListPartToPickUp[i].name.c_str());
    // }
    SmallerPath(RobotPosition,ListPartToPickUp,DeliveryOrderPoint );
    
  }
  catch(int NoFile) 
  {
    ROS_INFO("NO ORDER FOUND");
  }


    
  
  


}

int main(int argc, char **argv)
{


  ros::init(argc, argv, "OrderOptimizer");

  ros::NodeHandle n, n4;

 


  ros::Subscriber sub =  n.subscribe("nextOrder", 1000, chatterCallback);
  ros::Subscriber sub2 = n4.subscribe("currentPosition", 1000, PoseRobotCallback);
  Marker_pub= n.advertise<visualization_msgs::MarkerArray>("order_path", 1000);


  ros::spin();

  return 0;
}



